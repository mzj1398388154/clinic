-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 19, 2019 at 07:11 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `clinic`
--

-- --------------------------------------------------------

--
-- Table structure for table `announcement`
--

CREATE TABLE `announcement` (
  `id` int(11) NOT NULL,
  `title` text CHARACTER SET utf8 NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `code`
--

CREATE TABLE `code` (
  `id` int(11) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `ward` varchar(100) NOT NULL,
  `code_number` int(6) NOT NULL,
  `date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `code`
--

INSERT INTO `code` (`id`, `hospital`, `ward`, `code_number`, `date`) VALUES
(1, 'Atherton District Memorial Hospital', 'Atherton District Memorial Hospital', 718791, '2019-10-17'),
(2, 'Augathella Hospital', 'Nursing', 324389, '2019-10-17'),
(3, 'Belmont Private Hospital', 'Mental Health', 192639, '2019-10-17'),
(4, 'Belmont Private Hospital', 'Midwifery', 939666, '2019-10-17'),
(5, 'Blackwater Hospital', 'Nursing', 994904, '2019-10-17'),
(6, 'Boonah Hospital', 'Nursing', 985921, '2019-10-17'),
(7, 'Caboolture Hospital', 'Antenatal Clinic', 608937, '2019-10-17'),
(8, 'Caboolture Hospital', 'Antenatal Clinic - Kilcoy\r', 282857, '2019-10-17'),
(9, 'Caboolture Hospital', 'Antenatal Day Assessment Service', 770221, '2019-10-17'),
(10, 'Caboolture Hospital', 'Birth Suite', 694326, '2019-10-17'),
(11, 'Caboolture Hospital', 'Embrace Life', 646471, '2019-10-17'),
(12, 'Caboolture Hospital', 'Holding Hands Midwifery Group Practice', 322495, '2019-10-17'),
(13, 'Caboolture Hospital', 'Home Maternity Service', 768446, '2019-10-17'),
(14, 'Caboolture Hospital', 'Lucina Midwifery Group Practice', 937248, '2019-10-17'),
(15, 'Caboolture Hospital', 'Midwifery Group Practice - M & M', 272387, '2019-10-17'),
(16, 'Caboolture Hospital', 'Midwifery', 588311, '2019-10-17'),
(17, 'Caboolture Hospital', 'Ngarrama Midwifery Group Practice', 251586, '2019-10-17'),
(18, 'Caboolture Hospital', 'Post Natal Ward', 450959, '2019-10-17'),
(19, 'Caboolture Hospital', 'Special Care Nursery', 709422, '2019-10-17'),
(20, 'Charleville Hospital', 'Nursing', 960263, '2019-10-17'),
(21, 'Charleville Hospital', 'Health Clinic', 112070, '2019-10-17'),
(22, 'Cherbourg Hospital', 'Nursing', 735787, '2019-10-17'),
(23, 'Chinchilla Hospital', 'Nursing', 478297, '2019-10-17'),
(24, 'Cunnamulla Hospital', 'Nursing', 326973, '2019-10-17'),
(25, 'Dalby Hospital', 'Nursing', 863084, '2019-10-17'),
(26, 'Esk Health Service', 'Nursing', 802099, '2019-10-17'),
(27, 'Gatton Hospital', 'Gatton Hospital', 354542, '2019-10-17'),
(28, 'Goondiwindi Hospital', 'Midwifery', 669696, '2019-10-17'),
(29, 'Goondiwindi Hospital', 'Nursing', 708714, '2019-10-17'),
(30, 'Injune Hospital', 'Nursing', 815295, '2019-10-17'),
(31, 'Innisfail Hospital', 'Nursing', 968018, '2019-10-17'),
(32, 'Ipswich Hospital', 'Emergency Department', 304163, '2019-10-17'),
(33, 'Ipswich Hospital', 'Infection Control', 909328, '2019-10-17'),
(34, 'Ipswich Hospital', 'Intensive Care Unit', 438788, '2019-10-17'),
(35, 'Ipswich Hospital', 'Medical Imaging', 320551, '2019-10-17'),
(36, 'Ipswich Hospital', 'Midwifery', 734009, '2019-10-17'),
(37, 'Ipswich Hospital', 'Nurse Navigators', 322207, '2019-10-17'),
(38, 'Ipswich Hospital', 'Older Persons Mental Health Unit', 274583, '2019-10-17'),
(39, 'Ipswich Hospital', 'Operating Theatre', 943552, '2019-10-17'),
(40, 'Ipswich Hospital', 'Outpatient Department', 821514, '2019-10-17'),
(41, 'Ipswich Hospital', 'Quality and Safety Team', 897978, '2019-10-17'),
(42, 'Ipswich Hospital', 'Renal Dialysis Unit', 745299, '2019-10-17'),
(43, 'Ipswich Hospital', 'Ward 4E', 408048, '2019-10-17'),
(44, 'Ipswich Hospital', 'Ward 4F', 181578, '2019-10-17'),
(45, 'Ipswich Hospital', 'Ward 4G', 152771, '2019-10-17'),
(46, 'Ipswich Hospital', 'Ward 5F', 195702, '2019-10-17'),
(47, 'Ipswich Hospital', 'Ward 5G - Oncology', 472675, '2019-10-17'),
(48, 'Ipswich Hospital', 'Ward 6B', 644124, '2019-10-17'),
(49, 'Ipswich Hospital', 'Ward 7B', 874061, '2019-10-17'),
(50, 'Ipswich Hospital', 'Ward 7C', 694377, '2019-10-17'),
(51, 'Ipswich Hospital', 'Ward 7D', 236735, '2019-10-17'),
(52, 'Ipswich Hospital', 'Acute Care Team - Mental Health', 540512, '2019-10-17'),
(53, 'Ipswich Hospital', 'Acute Mental Health Unit', 924593, '2019-10-17'),
(54, 'Ipswich Hospital', 'Child Health', 648219, '2019-10-17'),
(55, 'Ipswich Hospital', 'Children\'s Sunshine Ward', 677865, '2019-10-17'),
(56, 'Ipswich Hospital', 'CNC Pain', 678996, '2019-10-17'),
(57, 'Ipswich Hospital', 'CNC Stroke', 839636, '2019-10-17'),
(58, 'Ipswich Hospital', 'CNC Wound Care', 921848, '2019-10-17'),
(59, 'Ipswich Hospital', 'Coronary Care Unit', 509322, '2019-10-17'),
(60, 'Ipswich Hospital', 'Emergency', 375569, '2019-10-17'),
(61, 'Kingaroy Hospital', 'Nursing', 779809, '2019-10-17'),
(62, 'Laidley Hospital', 'Nursing', 429728, '2019-10-17'),
(63, 'Logan Hospital', 'Antenatal Clinic', 658536, '2019-10-17'),
(64, 'Logan Hospital', 'Birth Suite', 553616, '2019-10-17'),
(65, 'Logan Hospital', 'Coronary Care Unit', 178036, '2019-10-17'),
(66, 'Logan Hospital', 'Day Therapy Unit', 728666, '2019-10-17'),
(67, 'Logan Hospital', 'Emergency Department', 656478, '2019-10-17'),
(68, 'Logan Hospital', 'Intensive Care Unit', 979343, '2019-10-17'),
(69, 'Logan Hospital', 'Maternity Assessment Clinic', 411658, '2019-10-17'),
(70, 'Logan Hospital', 'Midwifery', 256240, '2019-10-17'),
(71, 'Logan Hospital', 'Operating Theatre', 200213, '2019-10-17'),
(72, 'Logan Hospital', 'Outpatient Department', 392602, '2019-10-17'),
(73, 'Logan Hospital', 'Postnatal Home Visiting', 930100, '2019-10-17'),
(74, 'Logan Hospital', 'Pre-Op/Theatre', 975959, '2019-10-17'),
(75, 'Logan Hospital', 'Rehabilitation', 101298, '2019-10-17'),
(76, 'Logan Hospital', 'Renal Unit', 712508, '2019-10-17'),
(77, 'Logan Hospital', 'Special Care Nursery', 865230, '2019-10-17'),
(78, 'Logan Hospital', 'Ward 2A', 166178, '2019-10-17'),
(79, 'Logan Hospital', 'Ward 2B', 152252, '2019-10-17'),
(80, 'Logan Hospital', 'Ward 2C', 843328, '2019-10-17'),
(81, 'Logan Hospital', 'Ward 2D', 998257, '2019-10-17'),
(82, 'Logan Hospital', 'Ward 2F', 293236, '2019-10-17'),
(83, 'Logan Hospital', 'Ward 2G', 456966, '2019-10-17'),
(84, 'Logan Hospital', 'Ward 2GP', 735028, '2019-10-17'),
(85, 'Logan Hospital', 'Ward 2I', 517611, '2019-10-17'),
(86, 'Logan Hospital', 'Ward 3A', 356429, '2019-10-17'),
(87, 'Logan Hospital', 'Ward 3B', 657976, '2019-10-17'),
(88, 'Logan Hospital', 'Ward 3C', 771049, '2019-10-17'),
(89, 'Mareeba Hospital', 'Nursing', 996437, '2019-10-17'),
(90, 'Mater Childrens\' Private Hospital', 'MCPH', 592707, '2019-10-17'),
(91, 'Mater Cross Complex Services', 'Mater at Home', 147609, '2019-10-17'),
(92, 'Mater Hospital - Brisbane', '10A', 281782, '2019-10-17'),
(93, 'Mater Hospital - Brisbane', '10B - Oncology', 647408, '2019-10-17'),
(94, 'Mater Hospital - Brisbane', '8A - Orthopaedics', 448054, '2019-10-17'),
(95, 'Mater Hospital - Brisbane', '8B - Surgical', 884071, '2019-10-17'),
(96, 'Mater Hospital - Brisbane', '9A - Medical', 899417, '2019-10-17'),
(97, 'Mater Hospital - Brisbane', '9B - Medical', 159968, '2019-10-17'),
(98, 'Mater Hospital - Brisbane', 'Adolescent Drug and Alcohol Withdrawal Service', 221960, '2019-10-17'),
(99, 'Mater Hospital - Brisbane', 'Adult Emergency Department', 399043, '2019-10-17'),
(100, 'Mater Hospital - Brisbane', 'AOPS - Ambulatory Outpatient Services', 886234, '2019-10-17'),
(101, 'Mater Hospital - Brisbane', 'Coronary Care', 239626, '2019-10-17'),
(102, 'Mater Hospital - Brisbane', 'Day Surgery', 774962, '2019-10-17'),
(103, 'Mater Hospital - Brisbane', 'Diabetes Centre', 683649, '2019-10-17'),
(104, 'Mater Hospital - Brisbane', 'Intensive Care', 742294, '2019-10-17'),
(105, 'Mater Hospital - Brisbane', 'MAPU', 531163, '2019-10-17'),
(106, 'Mater Hospital - Brisbane', 'Mater Young Adults Health Centre Brisbane', 290425, '2019-10-17'),
(107, 'Mater Hospital - Brisbane', 'MICAH', 136382, '2019-10-17'),
(108, 'Mater Hospital - Brisbane', 'OPCCT - Older Person Centre Care Team', 769078, '2019-10-17'),
(109, 'Mater Hospital - Brisbane', 'Operating Theatre', 790439, '2019-10-17'),
(110, 'Mater Hospital - Brisbane', 'Short Stay Unit', 858981, '2019-10-17'),
(111, 'Mater Mothers\' Hospital', 'Antenatal Clinic', 532429, '2019-10-17'),
(112, 'Mater Mothers\' Hospital', 'Birth Suites', 925916, '2019-10-17'),
(113, 'Mater Mothers\' Hospital', 'Breastfeeding Support', 205792, '2019-10-17'),
(114, 'Mater Mothers\' Hospital', 'Coorparoo Young Women\'s Centre', 304695, '2019-10-17'),
(115, 'Mater Mothers\' Hospital', 'Home Care Program', 526410, '2019-10-17'),
(116, 'Mater Mothers\' Hospital', 'Maternal Fetal Medicine', 905641, '2019-10-17'),
(117, 'Mater Mothers\' Hospital', 'Midwifery Group Practice', 735348, '2019-10-17'),
(118, 'Mater Mothers\' Hospital', 'Midwifery', 806288, '2019-10-17'),
(119, 'Mater Mothers\' Hospital', 'Mothers\' Operating Rooms', 450872, '2019-10-17'),
(120, 'Mater Mothers\' Hospital', 'Neonatal Cardiac-Surgical (ICN2)', 663828, '2019-10-17'),
(121, 'Mater Mothers\' Hospital', 'Neonatal Preterm-Medical (ICN1)', 243531, '2019-10-17'),
(122, 'Mater Mothers\' Hospital', 'Pregnancy Assessment Centre', 680688, '2019-10-17'),
(123, 'Mater Mothers\' Hospital', 'Private Antenatal-Postnatal 10', 541612, '2019-10-17'),
(124, 'Mater Mothers\' Hospital', 'Private Antenatal-Postnatal 11', 610872, '2019-10-17'),
(125, 'Mater Mothers\' Hospital', 'Private Postnatal 12', 991186, '2019-10-17'),
(126, 'Mater Mothers\' Hospital', 'Public Antenatal 9', 698606, '2019-10-17'),
(127, 'Mater Mothers\' Hospital', 'Public Gynaecology 9', 730811, '2019-10-17'),
(128, 'Mater Mothers\' Hospital', 'Public Postnatal 8', 802324, '2019-10-17'),
(129, 'Mater Mothers\' Hospital', 'Special Care Nursery', 431320, '2019-10-17'),
(130, 'Mater Private Hospital - Brisbane', '10 East', 468352, '2019-10-17'),
(131, 'Mater Private Hospital - Brisbane', '10 North', 150957, '2019-10-17'),
(132, 'Mater Private Hospital - Brisbane', '8 East', 551102, '2019-10-17'),
(133, 'Mater Private Hospital - Brisbane', '8 North/East', 631359, '2019-10-17'),
(134, 'Mater Private Hospital - Brisbane', '8 South', 491222, '2019-10-17'),
(135, 'Mater Private Hospital - Brisbane', '9 East', 351265, '2019-10-17'),
(136, 'Mater Private Hospital - Brisbane', '9 North', 245626, '2019-10-17'),
(137, 'Mater Private Hospital - Brisbane', 'Anaesthetics', 536784, '2019-10-17'),
(138, 'Mater Private Hospital - Brisbane', 'CCU', 440912, '2019-10-17'),
(139, 'Mater Private Hospital - Brisbane', 'Endoscopy Unit', 211105, '2019-10-17'),
(140, 'Mater Private Hospital - Brisbane', 'Intensive Care Unit', 178810, '2019-10-17'),
(141, 'Mater Private Hospital - Brisbane', 'Mater Centre for Neurosciences', 625820, '2019-10-17'),
(142, 'Mater Private Hospital - Brisbane', 'Mater Private Emergency Care Centre', 748122, '2019-10-17'),
(143, 'Mater Private Hospital - Brisbane', 'Neuro OT', 288447, '2019-10-17'),
(144, 'Mater Private Hospital - Brisbane', 'Operating Theatres', 229927, '2019-10-17'),
(145, 'Mater Private Hospital - Brisbane', 'PACU', 755403, '2019-10-17'),
(146, 'Mater Private Hospital - Brisbane', 'Private Day Surgery', 418774, '2019-10-17'),
(147, 'Mater Private Hospital - Brisbane', 'Rehabilitation Level 4', 342591, '2019-10-17'),
(148, 'Mater Private Hospital - Brisbane', 'Rehabilitation Level 5', 835451, '2019-10-17'),
(149, 'Mater Private Hospital - Brisbane', 'Rehabilitation Level 6', 998435, '2019-10-17'),
(150, 'Mater Private Hospital - Brisbane', 'Scrub/Scout', 843580, '2019-10-17'),
(151, 'Mater Private Hospital - Brisbane', 'Wound Care', 167918, '2019-10-17'),
(152, 'Mater Private Hospital - Redland', 'Adult Medical PCU1', 643769, '2019-10-17'),
(153, 'Mater Private Hospital - Redland', 'Adult Surgical and Obstetrics PCU2', 315882, '2019-10-17'),
(154, 'Mater Private Hospital - Redland', 'Operating Rooms', 940026, '2019-10-17'),
(155, 'Mater Private Hospital - Springfield', 'Inpatient Unit 1', 895468, '2019-10-17'),
(156, 'Mater Private Hospital - Springfield', 'Inpatient Unit 2', 605351, '2019-10-17'),
(157, 'Mater Private Hospital - Springfield', 'OT Springfield', 217501, '2019-10-17'),
(158, 'Mitchell Hospital', 'Nursing', 691753, '2019-10-17'),
(159, 'Murgon Hospital', 'Nursing', 106008, '2019-10-17'),
(160, 'Princess Alexandra Hospital', 'Adult Acute Psychiatric Unit East Wing', 399931, '2019-10-17'),
(161, 'Princess Alexandra Hospital', 'Adult Acute Psychiatric Unit Grevillea', 251934, '2019-10-17'),
(162, 'Princess Alexandra Hospital', 'Adult Acute Psychiatric Unit North Wing', 925903, '2019-10-17'),
(163, 'Princess Alexandra Hospital', 'Adult Acute Psychiatric Unit West Wing', 490582, '2019-10-17'),
(164, 'Princess Alexandra Hospital', 'Adult Acute Psychiatric Unit', 712636, '2019-10-17'),
(165, 'Princess Alexandra Hospital', 'Brain Injury Rehabilitation Unit', 495320, '2019-10-17'),
(166, 'Princess Alexandra Hospital', 'Consultation Liaison', 987793, '2019-10-17'),
(167, 'Princess Alexandra Hospital', 'Coronary Care Unit', 256181, '2019-10-17'),
(168, 'Princess Alexandra Hospital', 'Dermatology Unit', 102939, '2019-10-17'),
(169, 'Princess Alexandra Hospital', 'Emergency Department', 721819, '2019-10-17'),
(170, 'Princess Alexandra Hospital', 'Geriatric and Rehabilitation Unit Banksia', 689606, '2019-10-17'),
(171, 'Princess Alexandra Hospital', 'Geriatric and Rehabilitation Unit Bunya', 505790, '2019-10-17'),
(172, 'Princess Alexandra Hospital', 'Geriatric and Rehabilitation Unit Cassia', 444727, '2019-10-17'),
(173, 'Princess Alexandra Hospital', 'Heart Recovery Program', 774941, '2019-10-17'),
(174, 'Princess Alexandra Hospital', 'Hepato Gastro Endoscopy', 791997, '2019-10-17'),
(175, 'Princess Alexandra Hospital', 'Inala Mental Health', 456710, '2019-10-17'),
(176, 'Princess Alexandra Hospital', 'Intensive Care Unit', 654180, '2019-10-17'),
(177, 'Princess Alexandra Hospital', 'Mental Health ED', 971378, '2019-10-17'),
(178, 'Princess Alexandra Hospital', 'Oncology Daycare', 791618, '2019-10-17'),
(179, 'Princess Alexandra Hospital', 'Operating Theatre', 107885, '2019-10-17'),
(180, 'Princess Alexandra Hospital', 'Radiation/Oncology', 553180, '2019-10-17'),
(181, 'Princess Alexandra Hospital', 'Spinal Injury Rehabilitation Unit', 948988, '2019-10-17'),
(182, 'Princess Alexandra Hospital', 'Ward 1C', 251600, '2019-10-17'),
(183, 'Princess Alexandra Hospital', 'Ward 1D', 986212, '2019-10-17'),
(184, 'Princess Alexandra Hospital', 'Ward 2A', 841039, '2019-10-17'),
(185, 'Princess Alexandra Hospital', 'Ward 2B', 417681, '2019-10-17'),
(186, 'Princess Alexandra Hospital', 'Ward 2C', 431410, '2019-10-17'),
(187, 'Princess Alexandra Hospital', 'Ward 2E', 598733, '2019-10-17'),
(188, 'Princess Alexandra Hospital', 'Ward 3C', 990021, '2019-10-17'),
(189, 'Princess Alexandra Hospital', 'Ward 4A', 389347, '2019-10-17'),
(190, 'Princess Alexandra Hospital', 'Ward 4BR', 298850, '2019-10-17'),
(191, 'Princess Alexandra Hospital', 'Ward 4C', 648848, '2019-10-17'),
(192, 'Princess Alexandra Hospital', 'Ward 4D', 625022, '2019-10-17'),
(193, 'Princess Alexandra Hospital', 'Ward 4E', 248168, '2019-10-17'),
(194, 'Princess Alexandra Hospital', 'Ward 5A', 629069, '2019-10-17'),
(195, 'Princess Alexandra Hospital', 'Ward 5B', 576017, '2019-10-17'),
(196, 'Princess Alexandra Hospital', 'Ward 5C', 256453, '2019-10-17'),
(197, 'Princess Alexandra Hospital', 'Ward 5D', 938425, '2019-10-17'),
(198, 'Princess Alexandra Hospital', 'Medical Assessment and Planning Unit', 533437, '2019-10-17'),
(199, 'Queen Elizabeth II Jubilee Hospital', 'Emergency Department', 693827, '2019-10-17'),
(200, 'Queen Elizabeth II Jubilee Hospital', 'Endoscopy', 607162, '2019-10-17'),
(201, 'Queen Elizabeth II Jubilee Hospital', 'Intensive Care Unit', 238712, '2019-10-17'),
(202, 'Queen Elizabeth II Jubilee Hospital', 'Operating Theatre', 179894, '2019-10-17'),
(203, 'Queen Elizabeth II Jubilee Hospital', 'Ward 2A', 961109, '2019-10-17'),
(204, 'Queen Elizabeth II Jubilee Hospital', 'Ward 2AM', 678737, '2019-10-17'),
(205, 'Queen Elizabeth II Jubilee Hospital', 'Ward 2B', 942845, '2019-10-17'),
(206, 'Queen Elizabeth II Jubilee Hospital', 'Ward 3A', 766037, '2019-10-17'),
(207, 'Queen Elizabeth II Jubilee Hospital', 'Ward 3B', 418335, '2019-10-17'),
(208, 'Queen Elizabeth II Jubilee Hospital', 'Ward 4B', 212786, '2019-10-17'),
(209, 'Queen Elizabeth II Jubilee Hospital', 'Ward 5B', 805939, '2019-10-17'),
(210, 'Queensland Children\'s Hospital', 'Emergency Department', 491041, '2019-10-17'),
(211, 'Queensland Children\'s Hospital', 'Operating Theatre', 157244, '2019-10-17'),
(212, 'Queensland Children\'s Hospital', 'Ward 10A', 764094, '2019-10-17'),
(213, 'Queensland Children\'s Hospital', 'Ward 10B', 564426, '2019-10-17'),
(214, 'Queensland Children\'s Hospital', 'Ward 11A', 573402, '2019-10-17'),
(215, 'Queensland Children\'s Hospital', 'Ward 11B', 237265, '2019-10-17'),
(216, 'Queensland Children\'s Hospital', 'Ward 1B', 741335, '2019-10-17'),
(217, 'Queensland Children\'s Hospital', 'Ward 2E', 653580, '2019-10-17'),
(218, 'Queensland Children\'s Hospital', 'Ward 3C', 895534, '2019-10-17'),
(219, 'Queensland Children\'s Hospital', 'Ward 3D', 303216, '2019-10-17'),
(220, 'Queensland Children\'s Hospital', 'Ward 4A - PICU', 579616, '2019-10-17'),
(221, 'Queensland Children\'s Hospital', 'Ward 4C', 340708, '2019-10-17'),
(222, 'Queensland Children\'s Hospital', 'Ward 5B', 278569, '2019-10-17'),
(223, 'Queensland Children\'s Hospital', 'Ward 5C', 418239, '2019-10-17'),
(224, 'Queensland Children\'s Hospital', 'Ward 5D', 775345, '2019-10-17'),
(225, 'Queensland Children\'s Hospital', 'Ward 5E', 285951, '2019-10-17'),
(226, 'Queensland Children\'s Hospital', 'Ward 6A', 854418, '2019-10-17'),
(227, 'Queensland Children\'s Hospital', 'Ward 6C', 150308, '2019-10-17'),
(228, 'Queensland Children\'s Hospital', 'Ward 6E', 601484, '2019-10-17'),
(229, 'Queensland Children\'s Hospital', 'Ward 8A', 720074, '2019-10-17'),
(230, 'Queensland Children\'s Hospital', 'Ward 8B', 926162, '2019-10-17'),
(231, 'Queensland Children\'s Hospital', 'Ward 9A', 177908, '2019-10-17'),
(232, 'Queensland Children\'s Hospital', 'Ward 9B', 384932, '2019-10-17'),
(233, 'Redcliffe Hospital', 'Transit Lounge', 552294, '2019-10-17'),
(234, 'Redcliffe Hospital', 'Antenatal Clinic', 707851, '2019-10-17'),
(235, 'Redcliffe Hospital', 'Antenatal Day Assessment Service', 115610, '2019-10-17'),
(236, 'Redcliffe Hospital', 'Birth Suite', 838929, '2019-10-17'),
(237, 'Redcliffe Hospital', 'Day Procedure Unit', 713264, '2019-10-17'),
(238, 'Redcliffe Hospital', 'Emergency Department', 167297, '2019-10-17'),
(239, 'Redcliffe Hospital', 'Home Maternity Service', 891400, '2019-10-17'),
(240, 'Redcliffe Hospital', 'Intensive Care Unit', 291964, '2019-10-17'),
(241, 'Redcliffe Hospital', 'Level 4 East', 751730, '2019-10-17'),
(242, 'Redcliffe Hospital', 'Level 4 West', 583902, '2019-10-17'),
(243, 'Redcliffe Hospital', 'Level 5 East', 711946, '2019-10-17'),
(244, 'Redcliffe Hospital', 'Level 5 West', 338240, '2019-10-17'),
(245, 'Redcliffe Hospital', 'Level 6 West', 331780, '2019-10-17'),
(246, 'Redcliffe Hospital', 'Medical Imaging', 455999, '2019-10-17'),
(247, 'Redcliffe Hospital', 'Midwifery Group Practice', 855074, '2019-10-17'),
(248, 'Redcliffe Hospital', 'Midwifery', 609953, '2019-10-17'),
(249, 'Redcliffe Hospital', 'Oncology Ward', 702815, '2019-10-17'),
(250, 'Redcliffe Hospital', 'Paediatrics Ward', 411645, '2019-10-17'),
(251, 'Redcliffe Hospital', 'Palliative Care Unit', 457229, '2019-10-17'),
(252, 'Redcliffe Hospital', 'Post Natal Ward', 367350, '2019-10-17'),
(253, 'Redcliffe Hospital', 'Rehabilitation Unit', 490301, '2019-10-17'),
(254, 'Redcliffe Hospital', 'Special Care Nursery', 857669, '2019-10-17'),
(255, 'Redcliffe Hospital', 'Specialist Outpatients Department', 861092, '2019-10-17'),
(256, 'Redland Hospital', 'Antenatal Clinic', 417807, '2019-10-17'),
(257, 'Redland Hospital', 'Birth Suite', 244801, '2019-10-17'),
(258, 'Redland Hospital', 'Canaipa Cardiac Ward', 669434, '2019-10-17'),
(259, 'Redland Hospital', 'Coronary Care Unit', 767374, '2019-10-17'),
(260, 'Redland Hospital', 'Emergency Department', 343131, '2019-10-17'),
(261, 'Redland Hospital', 'High Dependency Unit', 312914, '2019-10-17'),
(262, 'Redland Hospital', 'Karragarra - Postnatal Ward', 337743, '2019-10-17'),
(263, 'Redland Hospital', 'Lamb Children\'s Ward', 371789, '2019-10-17'),
(264, 'Redland Hospital', 'Macleay Medical Ward', 263355, '2019-10-17'),
(265, 'Redland Hospital', 'Midwifery', 348050, '2019-10-17'),
(266, 'Redland Hospital', 'Operating Theatre', 804978, '2019-10-17'),
(267, 'Redland Hospital', 'Postnatal Home Visiting', 215505, '2019-10-17'),
(268, 'Redland Hospital', 'Pre-Op Theatre', 811343, '2019-10-17'),
(269, 'Redland Hospital', 'Redland Midwifery Team', 972906, '2019-10-17'),
(270, 'Redland Hospital', 'Renal Unit', 382149, '2019-10-17'),
(271, 'Redland Hospital', 'Special Care Nursery', 677235, '2019-10-17'),
(272, 'Redland Hospital', 'Stradbroke Surgical Ward', 928764, '2019-10-17'),
(273, 'Rockhampton Base Hospital', 'Nursing', 130807, '2019-10-17'),
(274, 'Roma Hospital', 'Nursing', 863702, '2019-10-17'),
(275, 'Royal Brisbane and Women\'s Hospital', 'Alcohol and Drug Service', 851013, '2019-10-17'),
(276, 'Royal Brisbane and Women\'s Hospital', 'Birth Centre Caseload', 683311, '2019-10-17'),
(277, 'Royal Brisbane and Women\'s Hospital', 'Birth Suite', 315845, '2019-10-17'),
(278, 'Royal Brisbane and Women\'s Hospital', 'Cardiac Investigation Unit', 109885, '2019-10-17'),
(279, 'Royal Brisbane and Women\'s Hospital', 'Community Assessment and Referral Services [CARS]', 128506, '2019-10-17'),
(280, 'Royal Brisbane and Women\'s Hospital', 'Discharge Facilitation Unit (DFU)', 353771, '2019-10-17'),
(281, 'Royal Brisbane and Women\'s Hospital', 'Emergency Department', 389333, '2019-10-17'),
(282, 'Royal Brisbane and Women\'s Hospital', 'F Floor', 291978, '2019-10-17'),
(283, 'Royal Brisbane and Women\'s Hospital', 'G Floor', 723667, '2019-10-17'),
(284, 'Royal Brisbane and Women\'s Hospital', 'GE Unit - 9A Gastroenterology and Hepatology Department', 857718, '2019-10-17'),
(285, 'Royal Brisbane and Women\'s Hospital', 'H Floor', 510117, '2019-10-17'),
(286, 'Royal Brisbane and Women\'s Hospital', 'I Floor', 489004, '2019-10-17'),
(287, 'Royal Brisbane and Women\'s Hospital', 'Intensive Care Unit', 616483, '2019-10-17'),
(288, 'Royal Brisbane and Women\'s Hospital', 'Maternal Fetal Medicine', 571183, '2019-10-17'),
(289, 'Royal Brisbane and Women\'s Hospital', 'Midwifery', 977704, '2019-10-17'),
(290, 'Royal Brisbane and Women\'s Hospital', 'Mothers\' Outpatients Department', 436785, '2019-10-17'),
(291, 'Royal Brisbane and Women\'s Hospital', 'Ngarrama', 756734, '2019-10-17'),
(292, 'Royal Brisbane and Women\'s Hospital', 'PFU - Patient Flow Unit', 770569, '2019-10-17'),
(293, 'Royal Brisbane and Women\'s Hospital', 'PFU', 963827, '2019-10-17'),
(294, 'Royal Brisbane and Women\'s Hospital', 'Special Care Nursery', 891549, '2019-10-17'),
(295, 'Royal Brisbane and Women\'s Hospital', 'Ward 4C', 611200, '2019-10-17'),
(296, 'Royal Brisbane and Women\'s Hospital', 'Ward 6AN', 327491, '2019-10-17'),
(297, 'Royal Brisbane and Women\'s Hospital', 'Ward 6AS', 560823, '2019-10-17'),
(298, 'Royal Brisbane and Women\'s Hospital', 'Ward 6B', 933985, '2019-10-17'),
(299, 'Royal Brisbane and Women\'s Hospital', 'Ward 7AS', 856303, '2019-10-17'),
(300, 'Royal Brisbane and Women\'s Hospital', 'Ward 7BW', 556546, '2019-10-17'),
(301, 'Royal Brisbane and Women\'s Hospital', 'Ward 8AS', 149561, '2019-10-17'),
(302, 'Royal Brisbane and Women\'s Hospital', 'Ward 8BN', 572107, '2019-10-17'),
(303, 'Royal Brisbane and Women\'s Hospital', 'Ward 8BW', 598011, '2019-10-17'),
(304, 'Royal Brisbane and Women\'s Hospital', 'Ward 9AN', 378944, '2019-10-17'),
(305, 'Royal Brisbane and Women\'s Hospital', 'Ward 9AS', 641090, '2019-10-17'),
(306, 'Royal Brisbane and Women\'s Hospital', 'Ward 9BN', 121980, '2019-10-17'),
(307, 'Royal Brisbane and Women\'s Hospital', 'Ward 9BS', 501779, '2019-10-17'),
(308, 'Royal Brisbane and Women\'s Hospital', 'Ward 9N', 566971, '2019-10-17'),
(309, 'St Vincent\'s Private Hospital Northside', 'Cardiac Catheter Laboratory - CCL', 577483, '2019-10-17'),
(310, 'St Vincent\'s Private Hospital Northside', 'Day Oncology', 598743, '2019-10-17'),
(311, 'St Vincent\'s Private Hospital Northside', 'Day Surgery', 485617, '2019-10-17'),
(312, 'St Vincent\'s Private Hospital Northside', 'Emergency Centre', 109844, '2019-10-17'),
(313, 'St Vincent\'s Private Hospital Northside', 'Endoscopy', 840069, '2019-10-17'),
(314, 'St Vincent\'s Private Hospital Northside', 'Intensive Care Unit', 373235, '2019-10-17'),
(315, 'St Vincent\'s Private Hospital Northside', 'Operating Theatre/Recovery', 726946, '2019-10-17'),
(316, 'St Vincent\'s Private Hospital Northside', 'Paediatrics', 343790, '2019-10-17'),
(317, 'St Vincent\'s Private Hospital Northside', 'Palliative Care', 433864, '2019-10-17'),
(318, 'St Vincent\'s Private Hospital Northside', 'Ward 1A', 869718, '2019-10-17'),
(319, 'St Vincent\'s Private Hospital Northside', 'Ward 1B', 114413, '2019-10-17'),
(320, 'St Vincent\'s Private Hospital Northside', 'Ward 1C', 370384, '2019-10-17'),
(321, 'St Vincent\'s Private Hospital Northside', 'Ward 2B', 979350, '2019-10-17'),
(322, 'St Vincent\'s Private Hospital Northside', 'Ward 2C', 873917, '2019-10-17'),
(323, 'St Vincent\'s Private Hospital Northside', 'Ward FFC', 663274, '2019-10-17'),
(324, 'St Vincent\'s Private Hospital Northside', 'Ward FFN', 709876, '2019-10-17'),
(325, 'St Vincent\'s Private Hospital Northside', 'Ward FFS', 471282, '2019-10-17'),
(326, 'St Vincent\'s Private Hospital Northside', 'Ward GA', 710270, '2019-10-17'),
(327, 'St Vincent\'s Private Hospital Northside', 'Ward GB', 572829, '2019-10-17'),
(328, 'St Vincent\'s Private Hospital Northside', 'Ward GFN', 291670, '2019-10-17'),
(329, 'St Vincent\'s Private Hospital Northside', 'Ward GFS', 107950, '2019-10-17'),
(330, 'St Vincent\'s Private Hospital Northside', 'Ward SFN', 845074, '2019-10-17'),
(331, 'St Vincent\'s Private Hospital Northside', 'Ward SFS', 604114, '2019-10-17'),
(332, 'Stanthorpe Hospital', 'Nursing', 253195, '2019-10-17'),
(333, 'Sunnybank Private Hospital', 'Sunnybank Private Hospital', 891885, '2019-10-17'),
(334, 'Tara Hospital', 'Tara Hospital', 473996, '2019-10-17'),
(335, 'The Prince Charles Hospital', 'Coronary Care Unit', 510792, '2019-10-17'),
(336, 'The Prince Charles Hospital', 'Emergency Department', 623758, '2019-10-17'),
(337, 'The Prince Charles Hospital', 'Endoscopy', 545454, '2019-10-17'),
(338, 'The Prince Charles Hospital', 'Intensive Care Unit', 851012, '2019-10-17'),
(339, 'The Prince Charles Hospital', 'Paediatric Unit', 330403, '2019-10-17'),
(340, 'The Prince Charles Hospital', 'Post Anaesthetic Care Unit', 390020, '2019-10-17'),
(341, 'The Wesley Hospital', 'Ward 1E', 735490, '2019-10-17'),
(342, 'The Wesley Hospital', 'Ward 1K', 378814, '2019-10-17'),
(343, 'The Wesley Hospital', 'Ward 2M', 642420, '2019-10-17'),
(344, 'The Wesley Hospital', 'Ward MB1', 485502, '2019-10-17'),
(345, 'Toowong Private Hospital', 'Toowong Private Hospital', 673512, '2019-10-17'),
(346, 'St George Hospital', 'Nursing', 478862, '2019-10-17');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `hospital`, `name`, `email`) VALUES
(1, 'Redcliffe Hospital', 'Amanda Skyes', 'amanda.sykes@uq.edu.au'),
(2, 'RBWH Nursing', 'Carol Clark', 'carol.clark@uq.edu.au'),
(3, 'RBWH Nursing', 'Loretta Smith  ', 'l.smith1@uq.edu.au'),
(4, 'Prince Charles ', 'Sarah Hill', 's.hill@uq.edu.au'),
(5, 'QE2 Hospital ', 'Deb Evans', 'deborah.evans@uq.edu.au'),
(6, 'QE2 Hospital ', 'Jing Fang', 'jing.fang@uq.edu.au'),
(7, 'Logan Hospital ', 'Nicole Hunter', 'nicole.hunter@uq.edu.au'),
(8, 'Logan Hospital ', 'Neha Arora', 'n.arora@uq.edu.au'),
(9, 'Redland Hospital \r\n', 'Angus Bowie', 'angusjohnbowie@gmail.com'),
(10, 'Mater Redlands', 'Angus Bowie', 'angusjohnbowie@gmail.com'),
(11, 'Mater Springfield', 'Deb Evans', 'deborah.evans@uq.edu.au'),
(12, 'Mater South Brisbane', 'James Lee', 'james.lee2@uq.edu.au'),
(13, 'Mater South Brisbane', 'Felicity Spanevello', 'f.spanevello@uq.edu.au'),
(14, 'Mater South Brisbane', 'Ajitha Panicker', 'a.panicker@uq.edu.au'),
(15, 'Mater South Brisbane', 'Angela Cuskelly', 'a.cuskelly@uq.edu.au'),
(16, 'Mater South Brisbane', 'Renee Penwarden', 'r.penwarden@uq.edu.au'),
(17, 'PAH Mental Health ', 'Wonder Tapera  ', 'w.tapera@uq.edu.au'),
(18, 'PAH Mental Health ', 'Arvind Reddy Reddy', 'a.reddyreddy@uq.edu.au'),
(19, 'Community and Rural placements', 'Sarah Hill', 's.hill@uq.edu.au'),
(20, 'Ipswich Hospital', 'Alyce Stephenson', 'Alyce.Stephenson@health.qld.gov.au'),
(21, 'St Vincent\'s Private Hospital \r\n', 'Facilitators', 'svphnfacilitators@svha.org.au'),
(22, 'PAH', 'Lisa Atkin', 'l.atkin@uq.edu.au'),
(23, 'PAH', 'Jen Whitlock', 'j.whitlock@uq.edu.au'),
(24, 'PAH', 'Alyce Grant', 'alyce.grant@uq.edu.au'),
(25, 'PAH', 'Britt Miller', 'britt.miller@uq.edu.au'),
(26, 'PAH', 'Kerrie Rodger', 'k.rodger@uq.edu.au'),
(27, 'QCH', 'James Lee', 'jame.lee2@uq.edu.au'),
(28, 'QCH', 'Angela Cuskelly', 'a.cuskelly@uq.edu.au'),
(30, 'Blue Care Carina', 'Lesley Anderson', 'Lesley.andersen@uq.edu.au'),
(31, 'Blue Care Carramar', 'Allan Webber', 'allan.webber@uq.edu.au'),
(32, 'Blue Care Carramar', 'Carolyn Edwards', 'carolyn.edwards@uq.edu.au'),
(33, 'Blue Care Carramar', 'Vineeta Morgan', 'v.morgan@uq.edu.au'),
(34, 'COH Brighton/Kippa- Ring', 'Sacha Butler', 'sacha.butler@uq.edu.au'),
(35, 'COH Brighton/Kippa- Ring', 'Diane Pinkerton ', 'd.epinkerton@icloud.com'),
(39, 'Test', 'Test', 'Test@test.com');

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `email` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `ward` varchar(100) NOT NULL,
  `date` date NOT NULL,
  `buddy_nurse` varchar(100) NOT NULL,
  `start_time` float NOT NULL,
  `end_time` float NOT NULL,
  `lunch_time` float NOT NULL,
  `id` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `user_account`
--

CREATE TABLE `user_account` (
  `email` varchar(100) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `ward` varchar(100) NOT NULL,
  `already_read` tinyint(1) DEFAULT NULL,
  `annou_id_list` text,
  `course_code` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `announcement`
--
ALTER TABLE `announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `code`
--
ALTER TABLE `code`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `announcement`
--
ALTER TABLE `announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `code`
--
ALTER TABLE `code`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=347;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` float NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
