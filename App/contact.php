<html>
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type = "text/css" href="css/stylesheet.css">
</head>

<body style="margin-bottom:80px;text-align:center;">
<div class = "div1">
     
    <p class = "p">Contact Supervisor</p>
    
</div>

<?php
    include("remote/db.php");
    $db=new MySQLDatabase();
    $db->connect("webuser","","clinic");
    $sql="SELECT DISTINCT hospital FROM contact";
    $result = mysqli_query($db->link,$sql);
    while($data = mysqli_fetch_array($result)){
        $hos = $data['hospital'];
    ?>
        <div class="card" style="margin:3%;border-radius:1%;">
        <h5 class="card-header" style="font-size:17px;"><?php echo $hos?></h5>
        <div class="card-body" style="overflow:scroll;padding:2%;">
            <table class="table">
                <tr>
                <th style="width:90px;font-size:14px;border-top:none;">Name</th>
                <th style="font-size:14px;border-top:none;">Email</th>
                </tr>
                
                <?php
                    $str = str_replace("'","\'",$hos);
                    $sql="SELECT name,email FROM contact WHERE hospital='{$str}'";
                    $res = mysqli_query($db->link,$sql);
                    while($list = mysqli_fetch_array($res)){
                ?>
                    <tr>
                    <td style="width:90px;font-size:14px;"><?php echo $list["name"]; ?></td>
                    <td style="font-size:14px;"><?php echo $list["email"]; ?></td>
                    </tr>
                <?php
                    }
                ?>
                
            </table>
        </div>
        </div>
    <?php
        }
    ?>

 
<nav class="navbar fixed-bottom navbar-light" style="background-color: #ebebeb;padding-left:13%;padding-right:8%;height:55px;">
        <a class="navbar-brand" href="ShiftPicker.php" style="font-size:11px;"><img src="images/icon-shift.png" alt="" style="width:20px;height:20px;"><br/>SHIFT</a>
        <a class="navbar-brand" href="contact.php" style="font-size:11px;color:#0071BC"><img src="images/icon-contact1.png" alt="" style="width:20px;height:20px;"><br/>CONTACT</a>
        <a class="navbar-brand" href="announcement.php" style="font-size:11px;"><img src="images/icon-announcement.png" alt="" style="width:20px;height:20px;"><br/>ANNOU</a>
        <a class="navbar-brand" href="profile.php" style="font-size:11px;"><img src="images/icon-profile.png" alt="" style="width:20px;height:20px;"><br/>PROFILE</a>
</nav>
</body>
<script>
    checkRead();
    function checkRead(){
        $.ajax({
            type: "POST",
            url: "remote/check_read.php",
            data: "",
            crossDomain: true,
            cache: false,
            success: function(data) {
                if(data==1){
                    $(".navbar-brand:nth-child(3) img").attr("src","images/icon-re-announce.png");
                }
            }
        });
    }
</script>
</html>