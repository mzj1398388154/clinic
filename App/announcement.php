<html>

<head>
    <meta charset="utf-8" />
    <title>announcement</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" type = "text/css" href="css/stylesheet.css">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <style>
    nav{
        text-align:center;
    }

    .content{
        width:90%;
        padding:5px;
        margin:auto;
        margin-top:20px;;
        border:1px solid #750075;
    }

    .annou_date{
        color:gray;
        font-size:9pt;
        margin-top:0px;
        margin-bottom:0px;
        text-align:right;
    }
    
    .annou_content{
        color:black;
        margin-top:0px;
        color:black;
        font-size:10pt;
        margin-bottom:5px;
        text-align:left;
    }
    
    .annou_title{
        text-align:left;
        margin-top:0px;
        margin-bottom:5px;
        color:black;
        font-size:12pt;
        font-weight:bold;
    }
    </style>   
</head>
<body style="margin-bottom:80px;">

<div class = "div1">
     
    <p class = "p">Announcement</p>
    
</div>

<?php
    session_start();
    $email = $_SESSION['email'];
    include("remote/db.php");
    $db = new MySQLDatabase();
    $db->connect("webuser", "", "clinic");

    //set already ready to 0.
    $sql = "UPDATE user_account SET already_read=0 WHERE email='$email'";
    $result = mysqli_query($db->link, $sql);
    $sql = "SELECT annou_id_list FROM user_account WHERE email='$email'";
    $result = mysqli_query($db->link, $sql);
    $data = mysqli_fetch_array($result);
    $str = $data['annou_id_list'];
    $announList = explode(',',$str);
    $num = count($announList);
    for($i=$num-2;$i>=0;$i--){
        $sql = "SELECT title,content,date FROM announcement WHERE id = '$announList[$i]'";
        $result = mysqli_query($db->link, $sql);
        $data = mysqli_fetch_array($result);
    ?>
        <div class="content">
        <p class="annou_title"><?php echo $data['title'];?></p>
        <p class="annou_content"><?php echo $data['content'];?></p>
        <p class="annou_date">Date: <?php echo $data['date'];?></p>
        </div>
<?php }?>

<nav class="navbar fixed-bottom navbar-light" style="background-color: #ebebeb;padding-left:13%;padding-right:8%;height:55px;">
        <a class="navbar-brand" href="ShiftPicker.php" style="font-size:11px;"><img src="images/icon-shift.png" alt="" style="width:20px;height:20px;"><br/>SHIFT</a>
        <a class="navbar-brand" href="contact.php" style="font-size:11px;"><img src="images/icon-contact.png" alt="" style="width:20px;height:20px;"><br/>CONTACT</a>
        <a class="navbar-brand" href="announcement.php" style="font-size:11px;color:#0071BC"><img src="images/icon-announcement1.png" alt="" style="width:20px;height:20px;"><br/>ANNOU</a>
        <a class="navbar-brand" href="profile.php" style="font-size:11px;"><img src="images/icon-profile.png" alt="" style="width:20px;height:20px;"><br/>PROFILE</a>
</nav>

</body>

<script>
</script>
</html>