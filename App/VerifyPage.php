<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>Verification</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <style>
        .div1{
            background: linear-gradient(to top right, #66ccff 0%, #660066 100%);
            height: 50px;
            padding-top:10px;
        }
        .p{
            color: white;
            font-size: 1.2em;
            text-align: center;
            margin-top: 0%;
        }
    </style>
</head>

<body style="margin-bottom:300px;text-align:center;">

    <div class = "div1">
     
     <p class = "p">Verification</p>
     
    </div>
    <div class="Container" style="margin-top:18px;">
        <div class="month" style="margin-bottom: 50px">
            <ul>
                <li>
                    <span id ="day">0</span>
                    <br/>
                    <span id = "month">0</span>
                    <br/>
                    <span id = "date"  style="font-size:20px">0</span>
                </li>
            </ul>
        </div>
        <h4 id="duration" style=" color:  #5A0977;text-align: center; margin-bottom:20px;">
            0
        </h4>
        <h4 id="lunchtime" style=" color:  #5A0977;text-align: center;margin-bottom:20px;">
            0
        </h4>
        <form style="width:50%;position: absolute; left:25%; text-align:center;">
            <div class="form-group">
                <label for="name">Preceptor Name</label>
                <input type="name" class="form-control " id="name" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" required/>
            </div>
            <div class="form-group">
                <label for="pwd">Verification Code</label>
                <input type="password" class="form-control" id="pwd" autocomplete="off" readonly onfocus="this.removeAttribute('readonly');" required />
            </div>
            <button type="button" class="btn btn-primary " style="text-align: center;" onclick="verify()">
          SUBMIT
        </button>
        </form>
    </div>
    <nav class="navbar fixed-bottom navbar-light" style="background-color: #ebebeb;padding-left:13%;padding-right:8%;height:55px;">
        <a class="navbar-brand" href="ShiftPicker.php" style="font-size:11px;color:#0071BC"><img src="images/icon-shift1.png" alt="" style="width:20px;height:20px;"><br/>SHIFT</a>
        <a class="navbar-brand" href="contact.php" style="font-size:11px;"><img src="images/icon-contact.png" alt="" style="width:20px;height:20px;"><br/>CONTACT</a>
        <a class="navbar-brand" href="announcement.php" style="font-size:11px;"><img src="images/icon-announcement.png" alt="" style="width:20px;height:20px;"><br/>ANNOU</a>
        <a class="navbar-brand" href="profile.php" style="font-size:11px;"><img src="images/icon-profile.png" alt="" style="width:20px;height:20px;"><br/>PROFILE</a>
    </nav>
</body>
<script>
    checkRead();
    function checkRead(){
        $.ajax({
            type: "POST",
            url: "remote/check_read.php",
            data: "",
            crossDomain: true,
            cache: false,
            success: function(data) {
                if(data==1){
                    $(".navbar-brand:nth-child(3) img").attr("src","images/icon-re-announce.png");
                }
            }
        });
    }

    //display current time.
    var d = new Date();
    var days = {"0":"Sunday","1":"Monday","2":"Tuesday","3":"Wednesday","4":"Thursday","5":"Friday","6":"Saturday"};
    var months = {"0":"January","1":"February","2":"March","3":"April","4":"May","5":"June","6":"July","7":"Angust","8":"September","9":"October","10":"November","11":"December"};
    document.getElementById("month").innerHTML = months[d.getMonth()];
    document.getElementById("date").innerHTML = "Date"+d.getDate();
    document.getElementById("day").innerHTML = days[d.getDay()];

    var start_time = GetQueryString("startTime","");
    var end_time = GetQueryString("endTime","");
    document.getElementById("duration").innerHTML = "Duration: "+start_time+" to "+end_time;

    //convert time form to float form.
    start_time = start_time.replace(/:3/,".5");
    start_time = start_time.replace(/:0/,".0");
    end_time = end_time.replace(/:3/,".5");
    end_time = end_time.replace(/:0/,".0");
    var lunch_time = GetQueryString("lunchTime","");
    var lunch_time_display = "";
    if(lunch_time == "0"){
        lunch_time_display = "No lunchtime"
    }else if(lunch_time == "0.5"){
        lunch_time_display = "Lunchtime: 30 mins"
    }else{
        lunch_time_display = "Lunchtime: 1 hour"
    }
    document.getElementById("lunchtime").innerHTML = lunch_time_display;

    function GetQueryString(name,url){
        url = url || window.location.search.substr(1);
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = url.match(reg);
        if(r!=null)return decodeURI(r[2]);
        return null;
    }

    function verify(){
        var buddy_nurse = document.getElementById("name").value;
        if(buddy_nurse !=""){
            var code = document.getElementById("pwd").value;
            var dataString = "buddy_nurse="+buddy_nurse+"&code="+code+"&insert=";
            $.ajax({
            type: "POST",
            url:"remote/check_code.php",
            data: dataString,
            crossDomain: true,
            cache: false,
            success: function(data){
            if(data=="success")
            {
            alert("Verified");
            insert();
            }
            else
            {
            alert("Fail to verify!");
            }
            }
            });
        }else{
            alert("Buddy nurse name required");
        }
        
    }

    function insert(){
        var year_value = d.getFullYear();
        var months_b = {"0":"1","1":"2","2":"3","3":"4","4":"5","5":"6","6":"7","7":"8","8":"9","9":"10","10":"11","11":"12"};
        var month_value = months_b[d.getMonth()];
        var day_value = d.getDate();
        var date = year_value+"-"+month_value+"-"+day_value;
        var buddy_nurse = document.getElementById("name").value;
        var dataString = "start_time="+start_time+"&end_time="+end_time+"&lunch_time="+lunch_time+"&date="+date+"&buddy_nurse="+buddy_nurse+"&insert=";
        $.ajax({
        type: "POST",
        url:"remote/insert_shift.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        beforeSend: function(){},
        success: function(data){
        if(data=="success")
        {
        window.location.href="profile.php";
        }
        else if(data=="error")
        {
        alert("error");
        }
        }
        });
    }

</script>

</html>