<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/stylesheet.css">
    <style>
    main {
        font-size: 10pt;
        margin-top: 10px;
        display: flex;
        justify-content: center;
    }

    main>.item {
        flex: auto;
        margin-bottom: 5%;
        font-size: 15px;
    }

    table {
        text-align: center;
        width: 100%;
    }

    table td {
        padding: 3%;
        margin: 5%;
    }

    span {
        font-size: 10px;
        margin-left: 2%;
    }

    img {
        width: 30px;
        height: 30px;
    }

    .navi {
        padding-left: 13%;
        padding-right: 8%;
    }
    </style>
</head>

<body style="text-align:center;margin-bottom:20px;">
    <div class="div1">

        <p class="p">Profile</p>

    </div>
    <?php
    include("remote/db.php");
    $db = new MySQLDatabase();
    $db->connect("webuser", "", "clinic");
    session_start();
    $email = $_SESSION['email'];
    $first_name = $_SESSION['first_name'];
    $last_name = $_SESSION['last_name'];
    $name = $first_name . " " . $last_name;
    $hospital = $_SESSION['hospital'];
    $ward = $_SESSION['ward'];
    ?>
    <!-- The main content of prifile page, with displaying total hours, hospital info and shifts -->
    <div class="card">
        <a href="" style="font-size: 13pt;position: absolute;left: 30px;top: 10px;" data-toggle="modal"
            data-target="#SignUpModal">EDIT</a>
        <div class="modal fade" id="SignUpModal">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">


                    <div class="modal-header">
                        <h4 class="modal-title">Edit Profile</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>


                    <div class="modal-body">
                        <form>
                            <div class="form-group">
                                <label>Hospital</label><select type="hospital" id="newHospital" name="hospital"
                                    onchange="showWard();" class="form-control">
                                    <option value="" selected disabled hidden></option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Ward</label><br /><select type="ward" name="ward" id="newWard" class="form-control"></select>
                            </div>
                            <div class="form-group">
                                <label>Course</label><br />
                                <select type="course" name="course" id="newCourse" class="form-control">
                                    <option value="" selected disabled hidden></option>
                                    <option value="NURS1103">NURS1103</option>
                                    <option value="NURS1105">NURS1105</option>
                                    <option value="NURS2103">NURS2103</option>
                                    <option value="NURS2106">NURS2106</option>
                                    <option value="NURS3102">NURS3102</option>
                                    <option value="NURS3104">NURS3104</option>
                                </select>
                            </div>
                        </form>
                        <button type="submit" class="btn btn-primary" onclick="updateData()"
                            data-dismiss="modal">Update</button>
                    </div>
                </div>
            </div>
        </div>
        <img src="images/UQ.png" alt="UQ Logo" style="height:120px; width:120px;margin:auto; margin-top:10%;">
        <h1 style="margin-top:20px;font-size:15pt;" id="userName"><?php echo $name; ?></h1>

        <main>
            <!-- Retrive the data from the database and display. -->
            <div class="item" id="display"><?php echo $_SESSION['course']; ?><br /><?php echo $hospital; ?><br /><span><?php echo $ward; ?></span></div>
        </main>
        <main onclick="showHistory();">
            <?php
            $sql = "SELECT SUM(end_time-start_time-lunch_time) AS total_hour,COUNT('id') AS shifts_count FROM `shift` WHERE email = '$email'";
            $result = mysqli_query($db->link, $sql);
            while ($data = mysqli_fetch_array($result)) {
                ?>
            <div class="item" style="border-right:1px solid">Total Hours<br /><?php echo $data['total_hour']; ?></div>
            
            <div class="item" >Total Shifts<br /><?php echo $data['shifts_count']; ?></div>
            <?php } ?>
        </main>
    </div>
    <div class="card" style="border: none">

        <?php
        $sql = "SELECT * FROM shift WHERE email='{$email}'ORDER BY id DESC LIMIT 5";
        $result = mysqli_query($db->link, $sql);
        ?>
        <h5 style="margin:10px;">RECENT SHIFTS</h5>

        <table>
            <tr style="font-size:10pt;margin-bottom:10px;font-weight:bold;">
                <td>Start Time</td>
                <td>End Time</td>
                <td>Shift Date</td>
                <td>Buddy Nurse</td>
            </tr>
            <?php
            while ($data = mysqli_fetch_array($result)) {
                ?>
            <tr>
                <td><?php echo $data['start_time']; ?></td>
                <td><?php echo $data['end_time']; ?></td>
                <td><?php echo $data['date']; ?></td>
                <td><?php echo $data['buddy_nurse']; ?></td>
            </tr>
            <?php } ?>

        </table>
    </div>
    <div style="height:7%;position:relative;"></div>
</body>

<nav class="navbar fixed-bottom navbar-light navi" style="background-color: #ebebeb;height:55px;">
    <a class="navbar-brand" href="ShiftPicker.php" style="font-size:11px;color:black;" onclick="showChoose()"><img
            src="images/icon-shift.png" alt="" style="width:20px;height:20px;"><br />SHIFT</a>
    <a class="navbar-brand" href="contact.php" style="font-size:11px;color:black;"><img src="images/icon-contact.png"
            alt="" style="width:20px;height:20px;"><br />CONTACT</a>
    <a class="navbar-brand" href="announcement.php" style="font-size:11px;color:black;"><img
            src="images/icon-announcement.png" alt="" style="width:20px;height:20px;"><br />ANNOU</a>
    <a class="navbar-brand" href="" style="font-size:11px;color:#0071BC;"><img src="images/icon-profile1.png" alt=""
            style="width:20px;height:20px;"><br />PROFILE</a>
</nav>

<script type="text/javascript">
    $(document).ready(function () {
        checkRead();
        addHos();
    })

    function addHos(){
        $.ajax({
        url: 'hospital.json',
        async: false,
        success: function (data) {
            $.each (data, function (i, item)
            {
                $("#newHospital").append("<option value='"+item.hospital+"'>"+item.hospital+"</option>")
            });
        }
        });
    }

    function showWard(){
        var h_index = $("#newHospital").prop('selectedIndex');
        var input_ward = $("#newWard");
        input_ward.empty();
        $.ajax({
        url: 'hospital.json',
        async: false,
        success: function (data) {
            var ward_list = data[h_index-1].ward;
            for (i = 0; i < ward_list.length; i++){
                input_ward.append("<option value='"+ward_list[i]+"'>"+ward_list[i]+"</option>")
            }
        }
        });
    }
    function checkRead(){
        $.ajax({
            type: "POST",
            url: "remote/check_read.php",
            data: "",
            crossDomain: true,
            cache: false,
            success: function(data) {
                if(data==1){
                    $(".navbar-brand:nth-child(3) img").attr("src","images/icon-re-announce.png");
                }
            }
        });
    }
function showHistory() {
    window.location.href = "history.php";
}

function updateData() {
    var newhos = document.getElementById("newHospital").value;
    var newward = document.getElementById("newWard").value;
    var newcourse = document.getElementById("newCourse").value;
    var dataString = "hospital=" +
        newhos + "&ward=" + newward + "&course=" + newcourse + "&update=";
    $.ajax({
        type: "POST",
        url: "remote/update_data.php",
        data: dataString,
        crossDomain: true,
        cache: false,
        success: function(data) {
            var d = eval("(" + data + ")");
            $("#display").html(d.course+"<br />" + d.hospital + "<br /><span>" + d.ward + "</span>");
            alert("Update Success!");
        }
    });

}
</script>

</html>