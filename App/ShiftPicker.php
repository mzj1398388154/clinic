<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>shiftPicker</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <link rel="stylesheet" type="text/css" media="screen" href="css/styles.css" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <style>
        .div1{
            background: linear-gradient(to top right, #66ccff 0%, #660066 100%);
            height: 50px;
            padding-top:10px;
        }
        .p{
            color: white;
            font-size: 1.2em;
            text-align: center;
            margin-top: 0%;
        }
        .table td{
            padding:.55rem;
        }
    </style>
</head>

<body style="margin-bottom:120px;text-align:center;">

    <div class = "div1">
     
     <p class = "p">New Shift</p>
     
    </div>
    <div class="Container" style="margin-top:18px;">
        <div class="month">
            <ul>
                <li>
                    <span id ="day">0</span>
                    <br/>
                    <span id = "month">0</span>
                    <br/>
                    <span id = "date"  style="font-size:20px">0</span>
                </li>
            </ul>
        </div>
        <table class="table table-bordered" style="text-align: center;">
        <tbody style = "width:100%">
                <tr>
                    <td class="black" onclick="myFunction(this)">6:00</td>
                    <td class="black" onclick="myFunction(this)">6:30</td>
                    <td class="black" onclick="myFunction(this)">7:00</td>
                    <td class="black" onclick="myFunction(this)">7:30</td>
                    <td class="black" onclick="myFunction(this)">8:00</td>
                </tr>
                <tr>
                    <td class="black" onclick="myFunction(this)">8:30</td>
                    <td class="black" onclick="myFunction(this)">9:00</td>
                    <td class="black" onclick="myFunction(this)">9:30</td>
                    <td class="black" onclick="myFunction(this)">10:00</td>
                    <td class="black" onclick="myFunction(this)">10:30</td>
                </tr>
                <tr>
                    <td  class="black" onclick="myFunction(this)">11:00</td>
                    <td class="black" onclick="myFunction(this)">11:30</td>
                    <td class="black" onclick="myFunction(this)">12:00</td>
                    <td class="black" onclick="myFunction(this)">12:30</td>
                    <td class="black" onclick="myFunction(this)">13:00</td>
                </tr>
                <tr>
                    <td  class="black" onclick="myFunction(this)">13:30</td>
                    <td class="black" onclick="myFunction(this)">14:00</td>
                    <td class="black" onclick="myFunction(this)">14:30</td>
                    <td class="black" onclick="myFunction(this)">15:00</td>
                    <td class="black" onclick="myFunction(this)">15:30</td>
                </tr>
                <tr>
                    <td  class="black" onclick="myFunction(this)">16:00</td>
                    <td class="black" onclick="myFunction(this)">16:30</td>
                    <td class="black" onclick="myFunction(this)">17:00</td>
                    <td class="black" onclick="myFunction(this)">17:30</td>
                    <td class="black" onclick="myFunction(this)">18:00</td>
                </tr>
                <tr>
                    <td  class="black" onclick="myFunction(this)">18:30</td>
                    <td class="black" onclick="myFunction(this)">19:00</td>
                    <td class="black" onclick="myFunction(this)">19:30</td>
                    <td class="black" onclick="myFunction(this)">20:00</td>
                    <td class="black" onclick="myFunction(this)">20:30</td>
                </tr>
                <tr>
                    <td  class="black" onclick="myFunction(this)">21:00</td>
                    <td class="black" onclick="myFunction(this)">21:30</td>
                    <td class="black" onclick="myFunction(this)">22:00</td>
                    <td class="black" onclick="myFunction(this)">22:30</td>
                    <td class="black" onclick="myFunction(this)">23:00</td>
                </tr>
            </tbody>
        </table>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Lunch Time</span>
            </div>
        <select class="form-control" id="sel1">
          <option value="0">0</option>
          <option value="0.5">30 mins</option>
          <option value="1">1 hour</option>
        </select>
        </div>
        <input type="button" value="PROCEED TO VERIFICATION" class="btn btn-primary" onclick="submit()" style="text-align: center;">
    </div>

    <nav class="navbar fixed-bottom navbar-light" style="background-color: #ebebeb;padding-left:13%;padding-right:8%;height:55px;">
        <a class="navbar-brand" href="ShiftPicker.php" style="font-size:11px;"><img src="images/icon-shift1.png" alt="" style="width:20px;height:20px;"><br/>SHIFT</a>
        <a class="navbar-brand" href="contact.php" style="font-size:11px;"><img src="images/icon-contact.png" alt="" style="width:20px;height:20px;"><br/>CONTACT</a>
        <a class="navbar-brand" href="announcement.php" style="font-size:11px;"><img src="images/icon-announcement.png" alt="" style="width:20px;height:20px;"><br/>ANNOU</a>
        <a class="navbar-brand" href="profile.php" style="font-size:11px;"><img src="images/icon-profile.png" alt="" style="width:20px;height:20px;"><br/>PROFILE</a>
    </nav>
</body>
<script>
    checkRead();
    function checkRead(){
        $.ajax({
            type: "POST",
            url: "remote/check_read.php",
            data: "",
            crossDomain: true,
            cache: false,
            success: function(data) {
                if(data==1){
                    $(".navbar-brand:nth-child(3) img").attr("src","images/icon-re-announce.png");
                }
            }
        });
    }

    //display current time.
    var d = new Date();
    var days = {"0":"Sunday","1":"Monday","2":"Tuesday","3":"Wednesday","4":"Thursday","5":"Friday","6":"Saturday"};
    var months = {"0":"January","1":"February","2":"March","3":"April","4":"May","5":"June","6":"July","7":"Angust","8":"September","9":"October","10":"November","11":"December"};
    document.getElementById("month").innerHTML = months[d.getMonth()];
    document.getElementById("date").innerHTML = "Date"+d.getDate();
    document.getElementById("day").innerHTML = days[d.getDay()];

    function myFunction(elem) {
        elem.classList.toggle("red");
    }

    function grow(obj,url) {
    var s = '';
    for(var k in obj){
        s += "&"+k+"="+obj[k];
    }
    var Str = s.substr(1);

    if(url.indexOf("?") > -1){
        url = url+"&"+Str;
    }else{
        url = url+"?"+Str;
    }
    return url;
    }

    function submit() {

        //check if exactly two times have been selected.
        var list = document.getElementsByClassName("red");
        if (list.length > 2) {
            window.alert("Fail to submit: You have selected more than two times.");
        } else if (list.length == 2) {
            var start_time = document.getElementsByClassName("red")[0].innerHTML;
            var end_time = document.getElementsByClassName("red")[1].innerHTML;
            var e = document.getElementById("sel1");
            var lunch_time = e.options[e.selectedIndex].value;
    
            var currentData = {
            startTime:start_time,
            endTime:end_time,
            lunchTime:lunch_time
            };
            window.open( grow(currentData,"VerifyPage.php"),"_self");

        } else {
            window.alert("Fail to submit: Missing start time or end time.");
        }
    }
</script>

</html>