 <html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type = "text/css" href="css/stylesheet.css">
<script type="text/javascript">
    $(document).ready(function () {
        addHos();
    })
    function addHos(){
        $.ajax({
        url: 'hospital.json',
        async: false,
        success: function (data) {
            $.each (data, function (i, item)
            {
                $("#inputHospital").append("<option value='"+item.hospital+"'>"+item.hospital+"</option>")
            });
        }
        });
    }
    function showWard(){
        var h_index = $("#inputHospital").prop('selectedIndex');
        var input_ward = $("#inputWard");
        input_ward.empty();
        $.ajax({
        url: 'hospital.json',
        async: false,
        success: function (data) {
            var ward_list = data[h_index-1].ward;
            for (i = 0; i < ward_list.length; i++){
                input_ward.append("<option value='"+ward_list[i]+"'>"+ward_list[i]+"</option>")
            }
        }
        });
    }
</script>
</head>
<body class = "body">
    <?php
        session_start();
    ?>
    <form action="remote/signupdata.php" method="POST">
    <p>Clinical Placement</p>
    <center><img src = "images/UQ.png" alt = "UQ logo" width = "180px" height = "180px"></center>
    <p class = "p1" style = margin-top:5%>
        <?php 
            echo $_SESSION['email'];
        ?>
    </p>
    <p class = "p1">
        <?php 
            echo $_SESSION['first_name']." ";
        ?>
    </p>
    <p class = "p1">
        <?php 
            echo $_SESSION['last_name'];
        ?>
    </p>
    <li class = "li" style ="list-style-type: none; margin-top: 10%; float: left; margin-left : 10%;font-size:0.9em;"><label style = "color:white; font-size:0.9em; width:100%">Course Code:</label><select style = "background-color: rgba(0, 0, 0, 0.3); border-style:none; margin-bottom: 10px; margin-left:5px; color:white; width: 120%; font-size:1em" name="course_code" id="course_code" required></li>
    <option style = "color:white;" value="" selected disabled hidden >Course Code</option>
    <option value="NURS1103">NURS1103</option>
    <option value="NURS1105">NURS1105</option>
    <option value="NURS2103">NURS2103</option>
    <option value="NURS2106">NURS2106</option>
    <option value="NURS3102">NURS3102</option>
    <option value="NURS3104">NURS3104</option>
    </select></li>
    <li class = "li" style ="list-style-type: none; margin-top: 5%; float: left; margin-left : 1.5%; font-size:0.9em;"><label style = "color:white; font-size:0.9em;">Hospital:</label><select style = "background-color: rgba(0, 0, 0, 0.3); border-style:none; margin-bottom: 10px; margin-left:10px; color:white; width: 62%; font-size:1em" name="hospital" id="inputHospital" onchange="showWard()" required></li>
        
        <option style = "color:white;" value="" selected disabled hidden >Select Hospital</option>
      </select></li>
    <li class = "li" style ="margin-top: 5%; margin-bottom:10%; list-style-type: none; float: left; margin-left: 10%;font-size:0.9em;"><label style = "color:white; font-size:0.9em;">Ward:</label><select type="ward" name="ward" id="inputWard" style = "background-color: rgba(0, 0, 0, 0.3); color:white; border-style:none; margin-bottom: 10px; margin-left:10px; font-size:1em; width:80%;overflow-x:scroll;" required></li>

    </select></li>
    <button style = "margin-top: 35%" type="submit" class = "button1" id="signup_btn" value="signup">Sign Up</button>
    </form>
</body>
</html>
