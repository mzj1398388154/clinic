<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type = "text/css" href="css/stylesheet.css">
</head>

<body class = "body">
    <?php 
        require_once "uq/auth.php";
        auth_require();
        $uquser = auth_get_payload();
        $email = $uquser['email'];
        $full_name = $uquser['name'];
        $full_name_explode = explode(" ", $full_name);
        $first_name = $full_name_explode[0];
        $last_name = $full_name_explode[sizeof($full_name_explode)-1];
        session_start();
        $_SESSION['email']=$email;
        $_SESSION['first_name']=$first_name;
        $_SESSION['last_name']=$last_name;
    ?>

    <form method="post" action="remote/check_account.php">
    <p>Clinical Placement</p>
    <center><img src = "images/UQ.png" alt = "UQ logo" width = "180px" height = "180px"></center>
    <p class = "p1" style = margin-top:5%>
        <?php 
            echo $_SESSION['first_name']." ";
        ?>
        <?php 
            echo $_SESSION['last_name'];
        ?>
    </p>
    <button class = "button1" type="submit" value = "submit">SIGN IN</button>
    </form>
    <a style = "margin-right:10%; float:right;" class = "link" href = "signup.php">Sign Up</a>
</body>
</html>