<?php
include('header.php');
?>

<body>
<div class="form-group">
<label>Title:</label><input class ="form-control" type="text" id = "antitle">
<br>
<label>Content:</label>
<br>
<textarea class="form-control" type="text" id = "ancontent" row="10"></textarea>

<br />
<button class="btn btn-light mb-2" onclick = "spanList();">Select wards</button>
<div class="form-control" id = "m_choice">
</div>
<br>


<button class="btn btn-light mb-2" onclick = "spanCourse();">Select courses</button>
<div class="form-control " id="mulselect">
<ul id="course">
<li><input type="checkbox" id="checkbox" class="select_all">Select all</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS1103">NURS1103</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS1105">NURS1105</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS2103">NURS2103</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS2106">NURS2106</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS3102">NURS3102</li>
<li><input type="checkbox" id="checkbox" class= "cse" value="NURS3104">NURS3104</li>
</ul> 
</div>
<br>
<button id="annbut" class="btn btn-dark mb-2" type="button" onclick="submit()">Submit</button>
</div>
</body>

<script type="text/javascript">
    $(document).ready(function () {
        addHos();
        $("ul a").on("click",function(){
        if($(this).parent().children("li").css("display")=="none"){
            $(this).parent().children("li").css("display","block");
        }else{
            $(this).parent().children("li").css("display","none");
        }
        });

        $("input").on("click",function(){
            if($(this).val()=="hos"){
                if($(this).prop('checked')){
                    $(this).parent().children("li").children("input").attr("checked","checked");
                }else{
                    $(this).parent().children("li").children("input").attr("checked",false);
                }
            }
        });

        $(".select_all").on("click",function(){
            if($(this).prop('checked')){
                $("#course li").children("input").attr("checked","checked");
            }else{
                $("#course li").children("input").attr("checked",false);
            }
        });
    })
    function addHos(){

        $.ajax({
        url: 'hospital.json',
        async: false,
        success: function (data) {
            $.each (data, function (i, item)
            {
                var str = "<ul><input type=\"checkbox\" id=\"checkbox\"value=\"hos\" /><a>"+item.hospital+"</a>";
                var ward_list = item.ward;
                var id_list = item.id;
                for (i = 0; i < ward_list.length; i++){
                    str = str+"<li class=\"hosclass\"><input type=\"checkbox\" id=\"checkbox\" value=\""+id_list[i]+"\" />"+ward_list[i]+"</li>"
                }
                str = str + "</ul>";
                $("#m_choice").append(str);
                });
        }
        });
        
    }

function spanList(){
    if($("#m_choice").css("display")=="none"){
        $("#m_choice").css("display","block");
    }else{
        $("#m_choice").css("display","none");
    }
}

function spanCourse(){
    if($("#mulselect").css("display")=="none"){
        $("#mulselect").css("display","block");
    }else{
        $("#mulselect").css("display","none");
    }
}

function submit(){
    
    var title = $('#antitle').val();
    var content = $('#ancontent').val();
    var item = $(".cse");
    var course = "";
    for(var i = 0;i<item.length;i++){
        if(item[i].checked){
            course = course+"'"+item[i].value+"\',";
        }
    }
    course = course.substring(0,course.length-1)
    var wardData="";

    $(".hosclass").children("input").each(function(i,element){
        if($(element).prop('checked')){
            wardData = wardData + $(element).val() + ",";
        }
    })
    stringData = "title="+title+"&content="+content+"&course=("+course+")&ward=("+ wardData.substring(0,wardData.length-1) +")";
    $.ajax({
        type: "POST",
        url:"submit_an.php",
        data: stringData,
        crossDomain: true,
        cache: false,
        success: function(data){
            if(data==1) alert("success");
            $(location).attr('href',"announce.php");
        }
    }); 
}
</script>
</html>