<?php
    include('header.php');
    // Initialize a connection
    $conn = mysqli_connect('localhost', 'webuser', '') or die("error");
    if(!$conn)
        {
        die('Can not connect: ' . mysql_error());
        }
    //Select the database
    mysqli_select_db($conn, 'clinic');
    $query="SELECT title, content, date FROM announcement ORDER BY id DESC";
    $result=mysqli_query($conn,$query);
?>
<style>
    .content{
        width:90%;
        padding:5px;
        margin:auto;
        margin-top:20px;;
        border:1px solid #750075;
    }

    .annou_date{
        color:gray;
        font-size:9pt;
        margin-top:0px;
        margin-bottom:0px;
        text-align:right;
    }
    
    .annou_content{
        color:black;
        margin-top:0px;
        color:black;
        font-size:10pt;
        margin-bottom:5px;
        text-align:left;
    }
    
    .annou_title{
        text-align:left;
        margin-top:0px;
        margin-bottom:5px;
        color:black;
        font-size:12pt;
        font-weight:bold;
    }
</style>
    <table class="table">
        <tr id="anntitle">
            <th colspan="6"><h2>Announcement</h2><a class="btn btn-dark" href="newann.php">Create New Announcement</a></th>
        </tr>
        <tr>
            <th><h3>History Announcement</h3></th>
        </tr>
        <tr>
            <th>
            <?php
            while ($rows = mysqli_fetch_array($result))
            {?>
                <div class="content">
                <p class="annou_title"><?php echo $rows['title'];?></p>
                <p class="annou_content"><?php echo $rows['content'];?></p>
                <p class="annou_date"><?php echo $rows['date'];?></p>
                </div>
            <?php } ?>
            </th>
        </tr>
    </table>
</body>
</html>