<?php
    include('header.php');
    // Initialize a connection
    $conn = mysqli_connect('localhost', 'webuser', '');
    if(!$conn)
        {
        die('Can not connect: ' . mysql_error());
        }
    //Check if the staff search a student
    if(isset($_POST['email'])) {
            //search the student in shift db
            $email = $_POST['email'];
            mysqli_select_db($conn, 'clinic');
            $query="SELECT * FROM shift WHERE email = '$email'";
            $result = $conn->query($query);
            $total = 0;
                //Caululate the total hours of a student.
                while ($rows = mysqli_fetch_array($result)){
                    $hours = $rows['end_time'] - $rows['start_time'] - $rows['lunch_time'];
                    $total = $total + $hours;
                }
            //Get student name from the database
            $query="SELECT * FROM user_account WHERE email = '$email'"; 
            $students = $conn->query($query);
            while ($rows = mysqli_fetch_array($students)){
                $fname = $rows['first_name'];
                $lname = $rows['last_name'];
                $course= $rows['course_code'];
                }
              
        }
     //Check if the staff sort the students by course code   
     elseif(isset($_POST['course'])){
        $course = $_POST['course'];
        mysqli_select_db($conn, 'clinic');
        $query="SELECT * FROM user_account WHERE course_code = '$course'"; 
        $students = $conn->query($query);
        $query2="SELECT * FROM shift ";
        $shifts = $conn->query($query2);
        while($row2s = mysqli_fetch_array($shifts)){
            $records[]=$row2s;
        }
     }
    else{
        //Get all students shift hours from the database
        mysqli_select_db($conn, 'clinic');
        $query1="SELECT * FROM user_account "; 
        $students = $conn->query($query1);
        $query2="SELECT * FROM shift ";
        $shifts = $conn->query($query2);
        while($row2s = mysqli_fetch_array($shifts)){
            $records[]=$row2s;
        }
    }
?>

<table class="table">
        <thead>
        <tr>
            <th colspan="9" ><h2>Total Hours</h2></th>
        </tr>
         </thead>
         <thead>

        <!-- Search Student by email -->
        <tr>
        <form action='total.php' method='post'>
            <th>Search by student
            <input type="email" name="email" placeholder="Student email" class="form-control mt-3 mb-1" required>
            </th>
            <th>
            <button type="submit" class="btn btn-dark btn-block form-control mt-3 mb-1">Search</button>
            </th>
        </form> 
        </tr>

        <!-- Search students by course code -->
        <tr>
        <form action='total.php' method='post'>
            <th>Search by course code
            <select name="course" class="form-control mt-3 mb-1">
                <option value="" disabled selected>Select course code</option>
                <option value = 'NURS1103'>NURS1103</option>;
                <option value = 'NURS1105'>NURS1105</option>;
                <option value = 'NURS2103'>NURS2103</option>;
                <option value = 'NURS2106'>NURS2106</option>;
                <option value = 'NURS3102'>NURS3102</option>;
                <option value = 'NURS3104'>NURS3104</option>;
            </select>
            </th>
            <th>
            <button type="submit" class="btn btn-dark btn-block form-control mt-3 mb-1">Search</button>
            </th>
        </form> 
        </tr>
        </thead>
        
        <!-- Display the search or sort or all result of the students total hours -->
        <thead class="thead-dark">
        <tr>
            <th scope="col">Email</th>
            <th scope="col">First name</th>
            <th scope="col">Last name</th>
            <th scope="col">Course</th>
            <th scope="col">Total hours</th>
        </tr>
        </thead>
        <tbody>
        <?php
            //Check if select a certain student
            if(isset($email))
            {
                echo "<tr>";
                    echo "<td>" . $email . "</td>";
                    echo "<td>" . $fname . "</td>";
                    echo "<td>" . $lname . "</td>";
                    echo "<td>" . $course ."</td>";
                    echo "<td>" . $total . "</td>";  
                    echo "</tr>";              
            }
            elseif(isset($course)){
                while ($row1s = mysqli_fetch_array($students)){
                    $email = $row1s['email'];
                    $fname = $row1s['first_name'];
                    $lname = $row1s['last_name'];
                    $course = $row1s['course_code'];
                    $total = 0;
                    foreach($records as $row2s){
                        if($email==$row2s['email']){
                            $hours = $row2s['end_time'] - $row2s['start_time'] - $row2s['lunch_time'];
                            $total = $total + $hours;
                        }
                    }
                    echo "<tr>";
                    echo "<td>" . $email . "</td>";
                    echo "<td>" . $fname . "</td>";
                    echo "<td>" . $lname . "</td>";
                    echo "<td>" . $course ."</td>";
                    echo "<td>" . $total . "</td>";  
                    echo "</tr>";  
                }
            }           
            else{
                //Process all students shift data and display
                while ($row1s = mysqli_fetch_array($students)){
                    $email = $row1s['email'];
                    $fname = $row1s['first_name'];
                    $lname = $row1s['last_name'];
                    $course = $row1s['course_code'];
                    $total = 0;
                    foreach($records as $row2s){
                        if($email==$row2s['email']){
                            $hours = $row2s['end_time'] - $row2s['start_time'] - $row2s['lunch_time'];
                            $total = $total + $hours;
                        }
                    }
                    echo "<tr>";
                    echo "<td>" . $email . "</td>";
                    echo "<td>" . $fname . "</td>";
                    echo "<td>" . $lname . "</td>";
                    echo "<td>" . $course ."</td>";
                    echo "<td>" . $total . "</td>";  
                    echo "</tr>";  
                }
            }
        ?>
        </tbody>
