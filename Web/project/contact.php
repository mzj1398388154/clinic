<?php
    include('header.php');
    // Initialize a connection
    $conn = mysqli_connect('localhost', 'webuser', '');
    if(!$conn)
        {
        die('Can not connect: ' . mysql_error());
        }

        //Get contact list from the database
        mysqli_select_db($conn, 'clinic');
        if(isset($_POST['email'])){
            $email = $_POST['email'];
            $name = $_POST['name'];
            $hospital = $_POST['hospital'];
            $sql="INSERT INTO contact (hospital, name, email) VALUE ('$hospital', '$name', '$email')";
            if ($conn->query($sql) === TRUE) {
                header("Refresh:0; url=contact.php");
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;
            }
        }

?>

<table class="table">
        <thead>
        <tr>
            <th colspan="9" ><h2>Contact List</h2></th>
        </tr>
         </thead>
         <thead>
         <thead>

        <!-- Add new Contact-->
        <tr>
        <form action='contact.php' method='post'>
        <div class="row">
        <th scope="row">Add new Contact</th>
        </div>
        <div class="row">
            <th scope="col"><input type="email" name="email" placeholder="Email" class="form-control mt-3 mb-1" required></th>
            <th scope="col"><input type="text" name="name" placeholder="Name" class="form-control mt-3 mb-1" required></th>
            <th scope="col"><input type="text" name="hospital" placeholder="Hospital" class="form-control mt-3 mb-1" required></th>
            <th>
            <button type="submit" class="btn btn-dark btn-block form-control mt-3 mb-1">Add</button>
            </th>
        </div>
        </form> 
        </tr>
        <!-- Display the contact list -->
        <thead class="thead-dark">
        <tr>
            <th scope="col">Hospital</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
            <th scope="col">Operation</th>
        </tr>
        </thead>
        <tbody>
        <?php
            $query="SELECT * FROM contact ORDER BY hospital DESC"; 
            $result = $conn->query($query);
            while ($rows = mysqli_fetch_array($result))
            {?>
                <tr>
                <td><?php echo $rows['hospital'];?></td>
                <td><?php echo $rows['name'];?></td>
                <td><?php echo $rows['email'];?></td>
                <td><button class="btn btn-dark btn-sm delete" rel="<?php echo $rows['id'];?>">Delete</button>
                </tr>
            <?php } ?>
        </tbody>
    </table>
    <script>
    $( document ).ready(function() {

    $(".delete").click(function(){
        //select unique code and date cell
        
        var codeRow = $(this).parent().parent();
        
        var id = $(this).attr("rel");
        codeRow.fadeOut(300);
        $.ajax({
            type:'POST',
            url:'deleteCon.php',
            data: 'id='+id,
            cache:false,
            success:function(data){
            
            }
        });
    });
    });
</script>
</html>
