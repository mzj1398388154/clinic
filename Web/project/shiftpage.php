<?php
    include('header.php');
    // Initialize a connection
    $conn = mysqli_connect('localhost', 'webuser', '');
    if(!$conn)
        {
        die('Can not connect: ' . mysql_error());
        }
    //Define sort and order variable
    if(isset($_POST['sort'])){
        $sort = $_POST['sort'];
    }else{
        $sort = 'id';
    }
    if(isset($_POST['order'])){
        $order = $_POST['order'];
    }else{
        $order = 'DESC';
    }
    //Select the database
    mysqli_select_db($conn, 'clinic');
    $query="SELECT * FROM shift ORDER BY $sort $order";
    $result=mysqli_query($conn,$query);
?>
<body>
    <div class="overflow-auto">
    <table class="table">
        <thead>
        <tr>
            <th colspan="9" ><h2>Shifts</h2></th>
        </tr>
         </thead>
         <thead>
        <tr>
        <form action='shiftpage.php' method='post' class="form-control" >
            <th>Sort by
            <select name="sort" class="form-control mt-3 mb-1">
                <option value = 'id'>ID</option>
                <option value = 'hospital'>Hospital</option>
                <option value = 'ward'>Ward</option>
                <option value = 'email'>Email</option>
                <option value = 'date'>Date</option>
                <option value = 'buddy_nurse'>Buddy Nurse</option>
                <option value = 'start_time'>Start time</option>
                <option value = 'end_time'>End time</option>
                <option value = 'lunch_time'>Lunch time</option>
            </select>
            </th>
            <th>Order
            <select name='order' class="form-control mt-3 mb-1">
                <option value='DESC'>Descending</option>
                <option value='ASC'>Ascending</option>
            </select>
            </th>
            <th>
                <input class="btn btn-dark btn-block form-control mt-3 mb-1" type="submit" name='button' value='Sort'> 
            </th>
        </form> 
        </tr> 
        </thead>
        <thead class="thead-dark">
        <tr>
            <th scope="col">ID</th>
            <th scope="col">Email</th>
            <th scope="col">Hospital</th>
            <th scope="col">Ward</th>
            <th scope="col">Date</th>
            <th scope="col">Buddy nurse</th>
            <th scope="col">Start time</th>
            <th scope="col">End time</th>
            <th scope="col">Lunch time</th>
            <th scope="col">Operation</th>
        </tr>
        </thead>
        <tbody>
        <?php
        //Display the data from database and allows users to edit it
            while ($rows = mysqli_fetch_array($result))
            {?>
                <tr><form action=update.php method=post>
                <th scope='row'><input readonly class="readonly" name="id" type="text" value="<?php echo $rows['id'];?>"></th>
                <td><input readonly class="readonly" type=text name='email' value="<?php echo $rows['email'];?>"></td>
                <td><input readonly class="readonly" type=text name='hospital' value="<?php echo $rows['hospital'];?>"></td>
                <td><input readonly class="readonly" type=text name='ward' value="<?php echo $rows['ward'];?>"></td>
                <td><input readonly class="readonly" type=text name='date' value="<?php echo $rows['date'];?>"></td>
                <td><input readonly class="readonly" type=text name='buddy_nurse' value="<?php echo $rows['buddy_nurse'];?>"></td>
                <td><input type=text name='start_time' value="<?php echo $rows['start_time'];?>"></td>
                <td><input type=text name='end_time' value="<?php echo $rows['end_time'];?>"></td>
                <td><input type=text name='lunch_time' value="<?php echo $rows['lunch_time'];?>"></td>
                <td><button class="btn btn-dark btn-sm" type="submit">Submit</button></td>
                </form></tr>
           <?php }
        ?>
        </tbody>
    </table>
    </div>
</body>
</html>