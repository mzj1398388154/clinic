<!DOCTYPE html>
<html lang="en">
<head>
    <title>Clinical Placement</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />
    <link rel="stylesheet" href="css/style.css"/>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Latest compiled and minified CSS -->


<!-- Latest compiled and minified JavaScript -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/bootstrap-select.min.js"></script>

<!-- (Optional) Latest compiled and minified JavaScript translation files -->
<!-- <script src="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/js/i18n/defaults-*.min.js"></script> -->
</head>
<body>  

    <div class="sidenav">
        <div class="sidebar-header">
            <h3>Clinical Placement</h3>
        </div>
        <i class="fas fa-align-left"></i> <a href="shiftpage.php">Student Shift Time</a>
        <i class="fas fa-align-left"></i> <a href="total.php">Total Hours</a>
        <i class="fas fa-align-left"></i> <a href="announce.php">Announcement</a>
        <i class="fas fa-align-left"></i> <a href="codepage.php">Unique Code</a>
        <i class="fas fa-align-left"></i> <a href="contact.php">Contact</a>
        
    </div>
    <div class="wrapper">


</div>

    <header class="my-5 text-center">
        <h1 class="mx-3">Clinical Placement
        </h1>
    </header>