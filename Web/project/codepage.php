<?php
    include('header.php');
    // Initialize a connection
    $conn = mysqli_connect('localhost', 'webuser', '') or die("error");
    if(!$conn)
        {
        die('Can not connect: ' . mysql_error());
        }
    //Select the database
    mysqli_select_db($conn, 'clinic');
    $query="select * from code";
    $result=mysqli_query($conn,$query);
?>

<table class="table" >
        <thead>
        <tr>
            <td colspan="9"><h2>Unique code</h2></td>
        </tr>
        <tr><td> <form action=updateall.php><input class="btn btn-dark btn-lg btn-block" type=submit value='Update All'></form> </td></tr>
        </thead>
        <thead class="thead-dark">
            <th>ID</th>
            <th>Hospital</th>
            <th>Ward</th>
            <th>Unique code</th>
            <th>Date</th>
            <th>New code</th>
        </thead>
        <?php
            while ($rows = mysqli_fetch_array($result))
            {?>
                <tr>
                <td><input readonly class="readonly" type=text name=id value="<?php echo $rows['id'];?>"></td>
                <td><input readonly class="readonly" type=text name=hospital value="<?php echo $rows['hospital'];?>"></td>
                <td><input readonly class="readonly" type=text name=ward value="<?php echo $rows['ward'];?>"></td>
                <td><input readonly class="readonly" type=text name=code value="<?php echo $rows['code_number'];?>"></td>
                <td><input readonly class="readonly" type=text name=date value="<?php echo $rows['date'];?>"></td>
                <td><button type=submit rel="<?php echo $rows['id'];?>" class="btn btn-dark btn-sm newcode">UpdateCode</button>
                </tr>
            <?php } ?>
    </table>
</body>
<script>
    $( document ).ready(function() {

    $(".newcode").click(function(){
        //select unique code and date cell
        var codeRow = $(this);
        var id = codeRow.attr("rel");
        var codeCell = codeRow.parent().parent().find("td").eq(3).find("input");
        var dateCell = codeRow.parent().parent().find("td").eq(4).find("input");
        codeCell.fadeOut(300);
        dateCell.fadeOut(300);
        $.ajax({
            type:'POST',
            url:'updatecode.php',
            data:'id='+id,
            cache:false,
            success:function(data){
                //display new value
                var d = eval("("+data+")");
                codeCell.val(d.code_number);
                dateCell.val(d.date);
                dateCell.fadeIn(300);
                codeCell.fadeIn(300);
            }
        });
    });
    });
</script>
</html>